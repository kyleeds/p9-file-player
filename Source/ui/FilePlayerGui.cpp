/*
  ==============================================================================

    FilePlayerGui.cpp
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui (FilePlayer& filePlayer_) : filePlayer (filePlayer_)
{
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
    playbackSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    playbackSlider.setRange(0.f, 1.f);
    playbackSlider.addListener(this);
    addAndMakeVisible(playbackSlider);
    
    pitchSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    pitchSlider.setRange(0.01, 5.f);
    pitchSlider.setValue(1.f);
    pitchSlider.addListener(this);
    addAndMakeVisible(pitchSlider);
}

FilePlayerGui::~FilePlayerGui()
{
    delete fileChooser;
}


//Component
void FilePlayerGui::resized()
{
    playButton.setBounds (0, 0, getHeight()/3, getHeight()/3);
    fileChooser->setBounds (getHeight()/3, 0, getWidth()-getHeight(), getHeight()/3);
    playbackSlider.setBounds(0, 20, getWidth(), 20);
    pitchSlider.setBounds(0, 40, getWidth(), 20);
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        filePlayer.setPlaying(!filePlayer.isPlaying());
        if (filePlayer.isPlaying() == true)
        {
            startTimer(250);
        }
        else
        {
            stopTimer();
        }
        
    }
}

void FilePlayerGui::sliderValueChanged (Slider* slider)
{
    if (slider == &playbackSlider)
    {
        filePlayer.setPosition(playbackSlider.getValue());
    }
    if (slider == &pitchSlider)
    {
        filePlayer.setPlaybackRate(pitchSlider.getValue());
    }
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
                filePlayer.loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}
void FilePlayerGui::timerCallback()
{
    playbackSlider.setValue(filePlayer.getPosition());
}